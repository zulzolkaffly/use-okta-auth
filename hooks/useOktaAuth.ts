
import { OktaAuth, OktaAuthOptions } from "@okta/okta-auth-js";
import { useState, useEffect } from "react";
import config from "../okta-config";

interface authStateType {
  isPending: boolean,
  isAuthenticated: boolean,
  accessToken: any | undefined,
  idToken: any | undefined,
  refreshToken: any | undefined
};

const defaultState: authStateType = {
  isPending: true,
  isAuthenticated: false,
  accessToken: undefined,
  idToken: undefined,
  refreshToken: undefined
};

const oktaConfig: OktaAuthOptions = {
  ...config
};

const useOktaAuth = (): {
  authState: authStateType,
  oktaAuth: OktaAuth
} => {
  const oktaAuth = new OktaAuth(oktaConfig);
  const [authState, setAuthState] = useState(defaultState);

  useEffect(() => {
    oktaAuth.authStateManager.subscribe(authState => setAuthState(authState));
    oktaAuth.authStateManager.updateAuthState();
  }, []);

  return {
    authState,
    oktaAuth
  };
};

export default useOktaAuth;