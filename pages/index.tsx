import { useRouter } from "next/router";
import { useEffect } from "react";
import useOktaAuth from "../hooks/useOktaAuth";
import LoginForm from "../components/loginForm/LoginForm";

export default function Home() {
  const router = useRouter();
  const { authState } = useOktaAuth();
  const { isPending, isAuthenticated } = authState;

  useEffect(() => {
    if (isAuthenticated) {
      router.push("/clients");
      return;
    }
  }, [isAuthenticated]);

  return (
    <>
      {!isPending && !isAuthenticated && <LoginForm />}
    </>
  );
}
