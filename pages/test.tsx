import { useEffect } from "react";
import useOktaAuth from "../hooks/useOktaAuth";

export default function Test() {
  const { authState } = useOktaAuth();
  const { isPending } = authState;

  useEffect(() => {
    console.log(authState);
  }, [authState]);

  return (
    <>
      {!isPending && (
        <div>
          OKAY
        </div>
      )}
    </>
  );
}
