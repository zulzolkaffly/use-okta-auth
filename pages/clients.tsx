import { useEffect, useState } from "react";
import { OktaAuth } from "@okta/okta-auth-js";
import { useRouter } from "next/router";

export default function Clients() {
  const router = useRouter();

  const [isAuthenticated, setIsAuthenticated] = useState(null);

  const authClient = new OktaAuth({
    issuer: "https://dev-5484644.okta.com/oauth2/default",
    clientId: "0oa432ubnHH1AmIAC5d6",
    redirectUri: "http://localhost:3000/login/callback",
    pkce: true
  });

  useEffect(() => {
    const checkAuthentication = async () => {
      if (await authClient.isAuthenticated) {
        setIsAuthenticated(true);
        return;
      }

      router.push("/");
    };
    checkAuthentication();
  });

  return (
    <>
      {isAuthenticated && (
        <div className="bu">
          <main className="content box">
            <h1 className="title">Select Business Unit Id</h1>
            <br></br>
            <div className="field">
              <p className="control">
                <button
                  className="button is-success"
                  onClick={() => authClient.signOut()}
                >
                  Logout
                </button>
              </p>
            </div>
          </main>
        </div>
      )}
      <style jsx>{`
        .bu {
          display: flex;
          height: 100vh;
          align-items: center;
          justify-content: center;
        }
      `}</style>
    </>
  );
}
