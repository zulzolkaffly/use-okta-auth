import '../styles/globals.scss'
import '../styles/main.scss';

import MainLogo from "../components/mainLogo/MainLogo";
import Container from "../components/layouts/container/Container";

function MyApp({ Component, pageProps }) {
  return (
    <div className="main-wrapper">
      <div className="inner-wrapper">
        <div className="content-section">
          <Container>
            <MainLogo />
            <Component {...pageProps} />
          </Container>
        </div>
      </div>
    </div>
  );
}

export default MyApp
