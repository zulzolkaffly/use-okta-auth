import React from "react";
import Column from "../layouts/column/Column";
import Columns from "../layouts/columns/Columns";

import logo from "../../assets/images/logo-cheetah.svg";
import style from "./MainLogo.module.scss";

const MainLogo = () => (
  <Columns>
    <Column>
      <img className={style["main-logo"]} src={logo} alt="Cheetah Digital Logo" />
    </Column>
  </Columns>
);

export default MainLogo;
