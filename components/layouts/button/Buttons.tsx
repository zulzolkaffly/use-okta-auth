import React, { FC } from "react";
import classNames from "classnames";
import style from "./Button.module.scss";

type Props = {
  className?: string
};

const Buttons: FC<Props> = ({ children, ...props }) => {
  const { className } = props;
  const classes = classNames(style["cd"], style["buttons"], className);

  return <div className={classes}>{children}</div>;
};

export default Buttons;
