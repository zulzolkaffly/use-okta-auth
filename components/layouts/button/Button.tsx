import React, { FC } from "react";
import Link from "next/link";
import classNames from "classnames";
import A, { Props as AProps } from "../a/A";

import style from "./Button.module.scss";

enum COLOR {
  DEFAULT = "default",
  PRIMARY = "primary",
  WHITE = "white",
  DARK = "dark",
  DANGER = "danger",
  SUCCESS = "success",
  WARNING = "warning"
};

interface Props {
  disabled?: boolean;
  className?: string;
  href?: string;
  target?: AProps["target"];
  onClick?: React.MouseEventHandler<HTMLElement>;
  isText?: boolean;
  isFullwidth?: boolean;
  isRounded?: boolean;
  isPaddingless?: boolean;
  isDarker?: boolean;
  isOutlined?: boolean;
  isShadowless?: boolean;
  isIcon?: boolean;
  color?: COLOR.PRIMARY | COLOR.WHITE | COLOR.DANGER | COLOR.SUCCESS | COLOR.DEFAULT | "";
  leftIcon?: JSX.Element;
  rightIcon?: JSX.Element;
};

const Button: FC<Props> = ({
  children,
  className = "",
  href = "",
  target = "_blank",
  isText = false,
  isFullwidth = false,
  isRounded = false,
  isPaddingless = false,
  isShadowless = false,
  isDarker = false,
  isOutlined = false,
  isIcon = false,
  color = COLOR.PRIMARY,
  leftIcon = null,
  rightIcon = null,
  ...props
}) => {
  const classes = classNames(style["cd"], style["button"], className, {
    [style["is-text"]]: isText,
    [style["is-fullwidth"]]: isFullwidth,
    [style["is-rounded"]]: isRounded,
    [style["is-paddingless"]]: isPaddingless,
    [style["is-darker"]]: isDarker,
    [style["is-outlined"]]: isOutlined,
    [style["is-icon"]]: isIcon,
    [style["is-shadowless"]]: isShadowless,
    [style[`is-${color}`]]: color
  }, "button");

  const content = (
    <>
      {leftIcon}
      <span>{children}</span>
      {rightIcon && <span className="is-right">{rightIcon}</span>}
    </>
  );

  return (
    <>
      {href && !target && (
        <Link href={href}>
          <a href={href} className={classes} {...props}>
            {content}
          </a>
        </Link>
      )}
      {href && target && (
        <A href={href} target={target} className={classes} {...props}>
          {content}
        </A>
      )}
      {!href && (
        <a href={href} className={classes} {...props}>
          {content}
        </a>
      )}
    </>
  );
};

export default Button;
