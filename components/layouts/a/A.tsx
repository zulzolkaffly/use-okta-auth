import React, { FC } from "react";

export type Props = {
  href?: string,
  className?: string,
  target?: "_blank" | "_self" | "_parent" | "_top"
};

const A: FC<Props> = ({ target = "_blank", href = "", children, ...props }) => {
  return (
    <>
      {!href && <>{children}</>}
      {href && (
        <a {...props} rel="noopener noreferrer" target={target} href={href}>
          {children}
        </a>
      )}
    </>
  );
};

export default A;
