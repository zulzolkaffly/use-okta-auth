import React, { FC } from "react";
import classNames from "classnames";

import style from "./Container.module.scss";

type Props = {
  isFluid?: boolean
};

const Container: FC<Props> = ({ children, isFluid = false, ...props }) => {
  const className = classNames("cd", "container", style["container"], {
    "is-fluid": isFluid
  });

  return (
    <div className={className} {...props}>
      {children}
    </div>
  );
};

export default Container;
