import React, { forwardRef } from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import Icon from "../../elements/icon/Icon";
import SIZE from "../../utils/constant/size";
import "./Input.scss";

const InputText = forwardRef(
  ({ icon, onClick, className, error, ...props }, ref) => {
    const fieldClassName = classNames("cd", "field", className);

    const controlClassName = classNames("cd", "control", {
      "has-icons-right": icon
    });

    const inputClassName = classNames("cd", "input", {
      "is-danger": !!error
    });

    return (
      <div className={fieldClassName}>
        <p className={controlClassName}>
          <input ref={ref} className={inputClassName} type="text" {...props} />
          {icon && (
            <Icon
              name={icon}
              onClick={onClick}
              className="is-right"
              size={SIZE.SMALL}
            />
          )}
        </p>
        {error && <p className="help is-danger">{error}</p>}
      </div>
    );
  }
);

InputText.propTypes = {
  icon: PropTypes.node,
  maxLength: PropTypes.number,
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.string])
};

InputText.defaultProps = {
  icon: null,
  maxLength: 255,
  error: null
};

export default InputText;
