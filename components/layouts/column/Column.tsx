import React, { FC } from "react";
import classNames from "classnames";
import style from "./Column.module.scss";

type SomeType = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | null;

type Props = {
  isNarrow?: boolean,
  isOneFifth?: boolean,
  className?: string,
  size?: SomeType
};

const Column: FC<Props> = ({
  children,
  className = null,
  isNarrow = false,
  isOneFifth = false,
  size = null,
  ...props
}) => {
  const classes = classNames(style["cd"], style["column"], className, {
    [style["is-narrow"]]: isNarrow,
    [style["is-one-fifth"]]: isOneFifth,
    [style[`is-${size}`]]: size
  }, "column");

  return (
    <div {...props} className={classes}>
      {children}
    </div>
  );
};

export default Column;
