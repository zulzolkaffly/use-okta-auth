import React, { FC } from "react";
import classNames from "classnames";
import style from "./Label.module.scss";

interface Props {
  className?: string
};

const Label: FC<Props> = ({ children, className, ...props }) => {
  const classes = classNames(style["cd"], style["label"], "label", className);

  return (
    <div {...props} className={classes}>
      {children}
    </div>
  );
};

export default Label;
