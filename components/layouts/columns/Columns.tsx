import React, { FC } from "react";
import classNames from "classnames";
import style from "./Columns.module.scss";

type Props = {
  isFlexEnd?: boolean,
  isCenter?: boolean,
  isVcentered?: boolean,
  isMultiline?: boolean,
  isVbaselined?: boolean,
  isGapless?: boolean,
  isWrapRow?: boolean,
  isNoMarginBottom?: boolean,
  isMobile?: boolean,
  className?: string
};

const Columns: FC<Props> = ({
  children,
  isFlexEnd = false,
  isCenter = false,
  isVcentered = false,
  isMultiline = false,
  isVbaselined = false,
  isGapless = false,
  isWrapRow = false,
  isMobile = false,
  isNoMarginBottom = false,
  className = null,
  ...props
}) => {
  const classes = classNames(style["cd"], style["columns"], className, {
    [style["is-flex-end"]]: isFlexEnd,
    [style["is-center"]]: isCenter,
    [style["is-vcentered"]]: isVcentered,
    [style["is-multiline"]]: isMultiline,
    [style["is-vbaselined"]]: isVbaselined,
    [style["is-gapless"]]: isGapless,
    [style["is-wrap-row"]]: isWrapRow,
    [style["is-no-margin-bottom"]]: isNoMarginBottom,
    [style["is-mobile"]]: isMobile
  }, "columns");

  return (
    <div {...props} className={classes}>
      {children}
    </div>
  );
};

export default Columns;
