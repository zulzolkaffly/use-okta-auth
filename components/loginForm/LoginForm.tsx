import React, { useState, useEffect, useCallback } from "react";
import classNames from "classnames";
import { useTranslation } from "react-i18next";
import useOktaAuth from "../../hooks/useOktaAuth";
import Column from "../layouts/column/Column";
import Columns from "../layouts/columns/Columns";
import Button from "../layouts/button/Button";
import Label from "../layouts/label/Label";
import style from "./LoginForm.module.scss";

const LoginForm = () => {
  const [t] = useTranslation();
  const { oktaAuth } = useOktaAuth();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setLoading] = useState(false);
  const [isDisabled, setDisabled] = useState(true);
  const [error, setError] = useState(false);

  const errorLoginText = t("Error logging in with supplied credentials");

  const handleSubmit = useCallback((e) => {
    e.preventDefault();
    setLoading(true);

    oktaAuth
      .signInWithCredentials({ username, password })
      .then(res => {
        const sessionToken = res.sessionToken;
        oktaAuth.signInWithRedirect({ sessionToken });
        setTimeout(() => setLoading(false), 1000);
        setError(false);
      })
      .catch(() => {
        setError(true);
        setTimeout(() => setLoading(false), 1000);
      });
  }, [oktaAuth, password, username]);

  const handleUsernameChange = useCallback(e => {
    setUsername(e.target.value);
  }, []);

  const handlePasswordChange = useCallback(e => {
    setPassword(e.target.value);
  }, []);

  useEffect(() => {
    setDisabled(!username || !password);
  }, [username, password]);

  const classes = classNames(style["login-form"], {
    [style["is-error"]]: !!error
  })

  return (
    <div className={classes}>
      <Columns>
        <Column>
          <Label className={style["label"]}>{t("Username")}</Label>
          <input className={style["input"]} type="text" onChange={handleUsernameChange} />
        </Column>
      </Columns>
      <Columns>
        <Column>
          <Label>{t("Password")}</Label>
          <input className={style["input"]} type="password" onChange={handlePasswordChange} />
        </Column>
      </Columns>
      {!!error && (
        <Columns>
          <Column>
            <div className={style["error-msg"]}>{errorLoginText}</div>
          </Column>
        </Columns>
      )}
      <Columns>
        <Column>
          <div className={style["login-buttons"]}>
            <Button isFullwidth disabled={isDisabled || isLoading} onClick={handleSubmit}>
              {isLoading && <i className="fa fa-spinner fa-spin" />}
              {t("Login")}
            </Button>
          </div>
        </Column>
      </Columns>
    </div>
  );
};

export default LoginForm;